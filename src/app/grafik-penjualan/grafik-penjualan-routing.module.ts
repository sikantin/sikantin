import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GrafikPenjualanPage } from './grafik-penjualan.page';

const routes: Routes = [
  {
    path: '',
    component: GrafikPenjualanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GrafikPenjualanPageRoutingModule {}
