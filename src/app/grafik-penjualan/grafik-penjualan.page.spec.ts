import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GrafikPenjualanPage } from './grafik-penjualan.page';

describe('GrafikPenjualanPage', () => {
  let component: GrafikPenjualanPage;
  let fixture: ComponentFixture<GrafikPenjualanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrafikPenjualanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GrafikPenjualanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
