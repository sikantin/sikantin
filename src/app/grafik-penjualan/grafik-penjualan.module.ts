import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GrafikPenjualanPageRoutingModule } from './grafik-penjualan-routing.module';

import { GrafikPenjualanPage } from './grafik-penjualan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GrafikPenjualanPageRoutingModule
  ],
  declarations: [GrafikPenjualanPage]
})
export class GrafikPenjualanPageModule {}
