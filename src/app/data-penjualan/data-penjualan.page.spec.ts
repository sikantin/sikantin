import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DataPenjualanPage } from './data-penjualan.page';

describe('DataPenjualanPage', () => {
  let component: DataPenjualanPage;
  let fixture: ComponentFixture<DataPenjualanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataPenjualanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DataPenjualanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
