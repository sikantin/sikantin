import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DataPenjualanPageRoutingModule } from './data-penjualan-routing.module';

import { DataPenjualanPage } from './data-penjualan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DataPenjualanPageRoutingModule
  ],
  declarations: [DataPenjualanPage]
})
export class DataPenjualanPageModule {}
