import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataPenjualanPage } from './data-penjualan.page';

const routes: Routes = [
  {
    path: '',
    component: DataPenjualanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataPenjualanPageRoutingModule {}
