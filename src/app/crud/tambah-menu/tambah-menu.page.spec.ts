import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahMenuPage } from './tambah-menu.page';

describe('TambahMenuPage', () => {
  let component: TambahMenuPage;
  let fixture: ComponentFixture<TambahMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahMenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
