import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertController} from '@ionic/angular';

import firebase from '@firebase/app';
import '@firebase/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  email: string ="";
  password: string ="";

  constructor(public alert:AlertController, public router:Router) { }

  ngOnInit() {
  }

  async login() {
    firebase.auth().signInWithEmailAndPassword(this.email, this.password).then(
      (data)=>{
        this.showAlert("Login Berhasil", "Selamat Datang");
        console.log("Login Berhasil")
        this.router.navigate(["/home"])
      }
    ).catch((err)=>{
      this.showAlert("Login Gagal", err.message);
      console.log("Login Gagal")
    })
   
  }

  async showAlert(header:string, message:string) {
    const alert = await this.alert.create({
      header,
      message,
      buttons:["OK"]
    })
    await alert.present()
  }

}
