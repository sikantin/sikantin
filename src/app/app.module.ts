import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { firebase } from '@firebase/app';
var firebaseConfig = {
  apiKey: "AIzaSyAX2nvc-2dH7Xsdb_W5ymGbzG5b2s5WsTg",
  authDomain: "my-admin-60430.firebaseapp.com",
  projectId: "my-admin-60430",
  storageBucket: "my-admin-60430.appspot.com",
  messagingSenderId: "795824480682",
  appId: "1:795824480682:web:124bf371895ae9ae000bd3",
  measurementId: "G-RBEHHZEDCL"
};
firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
