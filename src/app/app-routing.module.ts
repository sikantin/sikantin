import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'edit-menu',
    loadChildren: () => import('./crud/edit-menu/edit-menu.module').then( m => m.EditMenuPageModule)
  },
  {
    path: 'tambah-menu',
    loadChildren: () => import('./crud/tambah-menu/tambah-menu.module').then( m => m.TambahMenuPageModule)
  },
  {
    path: 'data-penjualan',
    loadChildren: () => import('./data-penjualan/data-penjualan.module').then( m => m.DataPenjualanPageModule)
  },
  {
    path: 'grafik-penjualan',
    loadChildren: () => import('./grafik-penjualan/grafik-penjualan.module').then( m => m.GrafikPenjualanPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
